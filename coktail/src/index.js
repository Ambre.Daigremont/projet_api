import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './index.css';
//import App from './App';
import reportWebVitals from './reportWebVitals';
//import ListIngredient from "./ListIngredient";
import ListResultat from "./ListResultat";
import Recette from "./Recette";
import ListIngredient from './ListIngredient';

const rootElement = document.getElementById("root");
    ReactDOM.render(
      <BrowserRouter>
       <Switch>
        <Route exact path="/" component={ListIngredient} />
        <Route path="/recette/:id" component={Recette} />
        <Route path="/cocktail/:ingredient" component={ListResultat} />
      </Switch>
      </BrowserRouter>,
      rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

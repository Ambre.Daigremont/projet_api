/* src/App.js */

import React from "react";
import "./App.css";
import Header from "./Header";
import ListIngredient from "./ListIngredient";
import ListResultat from "./ListResultat";


function App() {
  return (
    <div className="Contour">
      <div className="App">
        <Header />
        <ListResultat />
      </div>
    </div>
  );
}

export default App;
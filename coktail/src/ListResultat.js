/* src/ListResultat.js 
Eline */

import React, { Component } from "react";
import {Link } from "react-router-dom";
import "./App.css";
import Header from "./Header";

const REQ = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=';
 
class ListResultat extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      hits: [],
      isLoading: false,
      error: null,
    };
  }
 
  componentDidMount() {
    this.setState({ isLoading: true });
    fetch (REQ + this.props.match.params.ingredient)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then(data => this.setState({ hits: data.drinks, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { hits, isLoading, error } = this.state;
 
    if (error) {
      return <p>Erreur : {error.message}</p>;
    }
    
    if (isLoading) {
      return <p>Loading ...</p>;
    }

  return (
    <div className="Contour">
    <div className="App">
    <Header />
      <div className="list-resultat">
          <h2>Cocktail list</h2>
          <h4>Ingredient : {this.props.match.params.ingredient}</h4>
            {hits.map(hit => (
            <Link to={'/recette/'+hit.idDrink}><button>
                {hit.strDrink} 
            </button></Link>
            ))}
      </div>
      <Link to="/"><button>
                return
    </button></Link>
    </div>
  </div>
  );
}
}

export default ListResultat;
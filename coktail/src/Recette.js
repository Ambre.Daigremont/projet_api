/* src/Recette.js 
Eline */

import React, { Component } from "react";
import {Link } from "react-router-dom";
import "./App.css";
import Header from "./Header";

const DRINK = 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=';
 
class Recette extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      hits: [],
      isLoading: false,
      error: null,
    };
  }
 
  componentDidMount() {
    this.setState({ isLoading: true });
    fetch (DRINK + this.props.match.params.id)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then(data => this.setState({ hits: data.drinks, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { hits, isLoading, error } = this.state;
 
    if (error) {
      return <p>Erreur : {error.message}</p>;
    }
    
    if (isLoading) {
      return <p>Loading ...</p>;
    }

  return (
    <div className="Contour">
    <div className="App">
    <Header />
    {hits.map(hit => (
      <div className="recette">
            <h2>{hit.strDrink}</h2>
            
            <img src={hit.strDrinkThumb} title={hit.strDrink} alt={hit.strDrink} width="250"></img>
            <h4>Ingrédients :</h4>
                <ul>
                    <li>{hit.strIngredient1}</li>
                    <li>{hit.strIngredient2}</li>
                    <li>{hit.strIngredient3}</li>
                    <li>{hit.strIngredient4}</li>
                    <li>{hit.strIngredient5}</li>
                    <li>{hit.strIngredient6}</li>
                    <li>{hit.strIngredient7}</li>
                    <li>{hit.strIngredient8}</li>
                    <li>{hit.strIngredient9}</li>
                    <li>{hit.strIngredient10}</li>
                    <li>{hit.strIngredient11}</li>
                    <li>{hit.strIngredient12}</li>
                    <li>{hit.strIngredient13}</li>
                    <li>{hit.strIngredient14}</li>
                    <li>{hit.strIngredient15}</li>
                </ul>
            <p>{hit.strInstructions}</p>
      </div>
      ))}
      <Link to="/"><button>
                return
    </button></Link>
    </div>
  </div>
  );
}
}

export default Recette;
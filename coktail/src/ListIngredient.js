/* src/ListIngredient.js 
Eline*/

import React from "react";
import "./App.css";
import {Link } from "react-router-dom";
import Header from "./Header";

const ingredients = ['Vodka', 'Gin', 'Rum', 'Tequila', 'Orange juice', '7-Up', 'Lemon juice', 'Red wine', 'Tonic water', 'Lime', 'Agave syrup', 'Passoa', 'Blue Curacao', 'Pineapple juice', 'Champagne', 'Jägermeister'];

class ListIngredient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ingredients[0], sesIngredients : ['Vodka', 'Orange juice'], isVisible : false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {

    let updatedList = this.sesIngredients.push(this.state.value)
    console.log(updatedList);
    this.setState({
      sesIngredients : updatedList
    });
    event.preventDefault();
   
  }

  render() {
    return (
    <div className="Contour">
    <div className="App">
    <Header />
      <div className="filter-list">
        <h2>Ingredient list</h2>
        <form onSubmit={this.handleSubmit}>
        <label>
          <select value={this.state.value} onChange={this.handleChange}>
            {ingredients.map(ingredient => (
                <option value={ingredient}>{ingredient}</option>
            ))}
          </select>
        </label>
        <input type="submit" value="Search" />
      </form>
      <h4>Mes ingrédients :</h4>
      <ul>
        {this.state.sesIngredients.map(sonIngredient => (
          <li key={sonIngredient}>{sonIngredient}</li>
        ))}
      </ul>
      </div>
    </div>
    </div>
    );
  }
}

export default ListIngredient;

/* 
Liste 
{ingredients.filter(ingredient => ingredient.includes('J')).map(filteredName => (
        <li>
          {filteredName}
        </li>
      ))}
      
<h4>Mes ingrédients :</h4>
      <ul>
        {this.state.sesIngredients.map(sonIngredient => (
          <li key={value}>{sonIngredient}</li>
        ))}
      </ul>


      <Link to={'/cocktail/'+this.state.value}><input type="submit" value="Search" /></Link>
*/